// console.log("Hello World!");

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
console.log("Original Array:")
console.log(users);

function addItem(WWE) {
  users.push(WWE);
}

addItem("John Cena");
console.log(users);

function getItemByIndex(index) {
    return users[index];
}
  
let itemFound = getItemByIndex(2);
console.log(itemFound);

function deleteItem() {
    let lastItem = users[users.length - 1];
    users.length--;
    return lastItem;
}
let deletedItem = deleteItem();
console.log(deletedItem);
console.log(users);

function updateItemByIndex(update, index) {
    users[index] = update;
  }
updateItemByIndex("Triple H", 3);
console.log(users);

function deleteAll() {
    users.length = 0;
}
  
  deleteAll();
  console.log(users);

function isEmpty(arr) {
    if (arr.length > 0) {
      return false;
    } else {
      return true;
    }
}
  
let isUsersEmpty = isEmpty(users);
console.log(isUsersEmpty); 



//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        users: typeof users !== 'undefined' ? users : null,
        addItem: typeof addItem !== 'undefined' ? addItem : null,
        getItemByIndex: typeof getItemByIndex !== 'undefined' ? getItemByIndex : null,
        deleteItem: typeof deleteItem !== 'undefined' ? deleteItem : null,
        updateItemByIndex: typeof updateItemByIndex !== 'undefined' ? updateItemByIndex : null,
        deleteAll: typeof deleteAll !== 'undefined' ? deleteAll : null,
        isEmpty: typeof isEmpty !== 'undefined' ? isEmpty : null,

    }
} catch(err){

}